﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float speed = 0.5f;
    public float x = 1;
    public float z = 0;
    public float rotationY = 90;

    void Update()
    {

        if (Input.GetKey(KeyCode.W))
        {
            transform.position = new Vector3(transform.position.x + ( speed * x ), transform.position.y, transform.position.z + ( speed * z ));
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            rotationY -= 90;
            transform.rotation = Quaternion.Euler(0, rotationY, 0);
            movementDirection();
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position = new Vector3(transform.position.x - ( speed * x ), transform.position.y, transform.position.z - ( speed * z ));
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            rotationY += 90;
            transform.rotation = Quaternion.Euler(0, rotationY, 0);
            movementDirection();
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + speed, transform.position.z);
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);
        }

    }

    private void movementDirection()
    {
        float remainder = rotationY % 360;
        if(remainder == 0)
        {
            x = 0;
            z = 1;
        }
        if (remainder == 270 || remainder == -90)
        {
            x = -1;
            z = 0;
        }
        if (remainder == 90 || remainder == -270)
        {
            x = 1;
            z = 0;
        }
        if (remainder == 180 || remainder == -180)
        {
            x = 0;
            z = -1;
        }

    }
}
