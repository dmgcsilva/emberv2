﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    public RandomGeneratorSync gen;

    public GameObject prefab;

    public GameObject playerCamera;

    [System.NonSerialized]
    private bool activated = false;

    [System.NonSerialized]
    private bool sceneReady = false;

    void Update()
    {
        if (!activated && sceneReady)
        {
            GameObject player = Instantiate(prefab, transform.position, transform.rotation);
            player.transform.parent = transform;
            playerCamera.transform.parent = player.transform;
            activated = true;
        }

        if (!sceneReady && gen.finished)
        {
            sceneReady = true;
        }
    }
}
