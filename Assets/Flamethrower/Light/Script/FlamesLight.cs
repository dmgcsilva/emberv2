﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamesLight : MonoBehaviour
{
    public float maxIntesity;
    public float normalIntesity;
    public float minIntesity;
    public float maxFlickerSpeed;
    public float minFlickerSpeed;
    public float onDuration;
    public float offDuration;

    public Light lightSource;

    private bool turningOn = false;
    private bool turningOff = false;
    private bool isOn = false;
    private float acum = 0;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            turningOn = true;
            turningOff = false;
            acum = 0;
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            turningOff = true;
            turningOn = false;
            acum = 0;
        }


        if(turningOn)
        {
            turnOnLight();
        }
        else if (turningOff)
        {
            turnOffLight();
        }
        else if (isOn)
        {
            flickerLight();
        }
    }

    private float currentSpeed = 0;

    private void flickerLight()
    {
        if (lightSource.intensity <= minIntesity*1.01f || currentSpeed == 0)
            currentSpeed = Random.Range(minFlickerSpeed, maxFlickerSpeed);

        lightSource.intensity = Mathf.Lerp(minIntesity, maxIntesity, Mathf.PingPong(Time.time, currentSpeed) / currentSpeed);
    }

    private void turnOnLight()
    {
        if (acum < 1)
        {
            acum += Time.deltaTime / onDuration;
        }

        float intesity = Mathf.Lerp(lightSource.intensity, normalIntesity, acum);
        lightSource.intensity = intesity;
        if (intesity >= minIntesity * 0.95)
        {
            turningOn = false;
            isOn = true;
        }
    }

    private void turnOffLight()
    {
        if (acum < 1)
        {
            acum += Time.deltaTime / offDuration;
        }

        float intesity = Mathf.Lerp(lightSource.intensity, 0, acum);
        lightSource.intensity = intesity;
        if (intesity <= 0.05)
        {
            turningOff = false;
            isOn = false;
        }
    }
}
