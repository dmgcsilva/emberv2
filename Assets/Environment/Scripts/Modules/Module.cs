﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

[System.Serializable]
public class Connection
{
    [SerializeField, ReadOnly] private Port port;
    [SerializeField, ReadOnly] private Module module;

    public Connection(Port port, Module module)
    {
        this.Port = port;
        this.Module = module;
    }

    public Port Port { get => port; private set => port = value; }
    public Module Module { get => module; private set => module = value; }
}

public class Module : MonoBehaviour
{
    [SerializeField, ReadOnly] private List<Connection> connections;

    [SerializeField, HideInInspector] private bool isAdapter;
    [SerializeField, HideInInspector] private bool isBounded;
    [SerializeField, HideInInspector] private Vector3 bounds;


    public List<Connection> Connections
    {
        get => connections != null ? new List<Connection>(connections) : new List<Connection>();
        private set => connections = value != null ? new List<Connection>(value) : null;
    }

    public void addConnection(Connection connetion)
    {
        connections.Add(connetion);
    }

    public Connection removeConnection(Module other)
    {
        for (int i=0; i < connections.Count; i++)
        {
            Connection connection = connections[i];
            if (connection.Module.gameObject.Equals(other.gameObject))
            {
                connections.RemoveAt(i);
                return connection;
            }
        }
        return null;
    }

    public bool IsAdapter { get => isAdapter; private set => isAdapter = value; }
    public bool IsBounded { get => isBounded; private set => isBounded = value; }
    public Vector3 Bounds { get => bounds; private set => bounds = value; }




#if UNITY_EDITOR

    [PropertySpace(SpaceBefore = 15, SpaceAfter = 5)]
    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }



    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public Module target;

        public Inspector(Module target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {

        }

        public void sync()
        {
            isAdapter = target.IsAdapter;
            isBounded = target.IsBounded;
            bounds = target.Bounds;
        }

        public void save()
        {
            target.isAdapter = isAdapter;
            target.isBounded = isBounded;
            target.Bounds = bounds;
        }

        [LabelWidth(70)]
        public bool isAdapter;

        [LabelWidth(70)]
        [HorizontalGroup("Split")]
        [PropertySpace(SpaceAfter = 5)]
        public bool isBounded;

        [LabelWidth(60)]
        [HorizontalGroup("Split")]
        [ShowIf("isBounded")]
        [PropertySpace(SpaceAfter = 5)]
        public Vector3 bounds;

        [LabelWidth(70)]
        [BoxGroup("Module Set")]
        [ValueDropdown("@EnvironmentUtils.getAssetDropdown<ModuleSet>()")]
        public ModuleSet set = null;

        [LabelWidth(70)]
        [BoxGroup("Module Set")]
        [DrawWithUnity]
        public AssetReference reference = null;

        [BoxGroup("Module Set")]
        [Button]
        public void registerInSet()
        {
            set.registerModule(target, reference, isAdapter);
        }
    }

#endif
}
