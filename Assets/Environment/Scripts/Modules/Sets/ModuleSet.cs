﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System;

[System.Serializable]
public struct TaggedPort
{
    [SerializeField] private Vector3 localPosition;
    [SerializeField] private Quaternion localRotation;
    [SerializeField] private PortType portType;

    public TaggedPort(Vector3 localPosition, Quaternion localRotation, PortType portType) : this()
    {
        this.LocalPosition = localPosition;
        this.LocalRotation = localRotation;
        this.PortType = portType;
    }

    public Vector3 LocalPosition { get => localPosition; set => localPosition = value; }
    public Quaternion LocalRotation { get => localRotation; set => localRotation = value; }
    public PortType PortType { get => portType; set => portType = value; }
}

[System.Serializable]
public struct TaggedModule
{
    [SerializeField] private GameObject module;
    [SerializeField] private AssetReference reference;
    [SerializeField] private List<TaggedPort> ports;

    public TaggedModule(Module module, AssetReference reference, bool isAdapter) : this()
    {
        this.Module = module.gameObject;

        ports = new List<TaggedPort>();
        foreach(Transform portTransform in module.transform.Find("Ports"))
        {
            Port port = portTransform.GetComponent<Port>();
            ports.Add(new TaggedPort(port.transform.localPosition, port.transform.localRotation, port.Type));
        }

        this.Reference = reference;
    }

    public List<TaggedPort> Ports {
        get => ports != null ? new List<TaggedPort>(ports) : new List<TaggedPort>();
        private set => ports = value != null ? new List<TaggedPort>(value) : null;
    }

    public AssetReference Reference { get => reference; private set => reference = value; }
    public GameObject Module { get => module; set => module = value; }
}

[CreateAssetMenu(fileName = "ModuleSet", menuName = "Procedural/Module/Set")]
public class ModuleSet : ScriptableObject
{
    [SerializeField] private List<TaggedModule> modules;

    public List<TaggedModule> Modules {
        get => modules != null ? new List<TaggedModule>(modules) : new List<TaggedModule>();
        private set => modules = value != null ? new List<TaggedModule>(value) : null;
    }

    public void registerModule(Module module, AssetReference reference, bool isAdapter)
    {
        modules.Add(new TaggedModule(module,reference,isAdapter));
    }
}
