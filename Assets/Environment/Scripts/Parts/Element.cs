﻿using UnityEngine;

public class Element : MonoBehaviour
{
    [SerializeField] private bool canOverlap = false;

    public bool CanOverlap { get => canOverlap; private set => canOverlap = value; }

    void Awake()
    {
        int variant = UnityEngine.Random.Range(0, transform.childCount);
        for (int i = 0; i < transform.childCount; i++)
        {
            if (i == variant)
                transform.GetChild(i).gameObject.SetActive(true);
            else
                transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
