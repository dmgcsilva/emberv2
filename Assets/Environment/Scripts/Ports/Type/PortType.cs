﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public enum Direction { Horizontal, Vertical }

[CreateAssetMenu(fileName = "Type", menuName = "Procedural/Ports/Type")]
public class PortType : ScriptableObject 
{

    [SerializeField, HideInInspector] private Direction direction;
    [SerializeField, HideInInspector] private List<Plug> plugs;
    [SerializeField, HideInInspector] private bool isAdapterPort;

    public Direction Direction { get => direction; private set => direction = value; }

    public List<Plug> Plugs
    {
        get => plugs != null ? new List<Plug>(plugs) : new List<Plug>();
        private set => plugs = value != null ? new List<Plug>(value) : null;
    }
    public bool IsAdapterPort { get => isAdapterPort; set => isAdapterPort = value; }




#if UNITY_EDITOR


    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }



    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public PortType target;

        public Inspector(PortType target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {

        }

        public void sync()
        {
            direction = target.Direction;
            plugs = target.Plugs;
            isAdapterPort = target.IsAdapterPort;
        }

        public void save()
        {
            target.Direction = direction;
            target.Plugs = plugs;
            target.IsAdapterPort = isAdapterPort;
        }

        public Direction direction;
        [ValueDropdown("@EnvironmentUtils.getAssetDropdown<Plug>()", ExcludeExistingValuesInList = true)]
        public List<Plug> plugs;
        [LabelWidth(100)]
        public bool isAdapterPort;
    }

#endif
}