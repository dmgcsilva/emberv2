﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;

[System.Serializable]
public struct Format
{
    [SerializeField, HideInInspector] private GameObject part;
    [SerializeField, HideInInspector] private MappingPortPart mapping;

    public Format(GameObject part, MappingPortPart mapping) : this()
    {
        this.Part = part;
        this.Mapping = mapping;
    }

    public GameObject Part { get => part; set => part = value; }
    public MappingPortPart Mapping { get => mapping; set => mapping = value; }
}

public class Port : MonoBehaviour
{
    [SerializeField, HideInInspector] private PortType type;
    [SerializeField, HideInInspector] private List<Format> formats;

    public PortType Type { get => type; private set => type = value; }
    public List<Format> Formats
    {
        get => formats != null ? new List<Format>(formats) : new List<Format>();
        private set => formats = value != null ? new List<Format>(value) : null;
    }




#if UNITY_EDITOR


    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }


    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public Port target;

        public Inspector(Port target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {
           
        }

        public void sync()
        {
            type = target.Type;
            formats = target.Formats.Select(t => new _Format(t)).ToList();
        }

        public void save()
        {
            target.Type = type;
            target.Formats = formats.Select(t => t.convert()).ToList();
        }

        [ValueDropdown("@EnvironmentUtils.getAssetDropdown<PortType>()")]
        public PortType type;

        [ListDrawerSettings(Expanded = true)]
        public List<_Format> formats;


        public struct _Format
        {
            [ValueDropdown("@EnvironmentUtils.getAllModuleParts(($property.Tree.WeakTargets[0] as Port).transform.parent.parent.gameObject)", ExcludeExistingValuesInList = true)]
            public GameObject part;

            
            [ValueDropdown("@getMapping(($property.Tree.WeakTargets[0] as Port))")]
            public MappingPortPart mapping; 

            public _Format(GameObject part, MappingPortPart mapping)
            {
                this.part = part;
                this.mapping = mapping;
            }

            public _Format(Format target)
            {
                this.part = target.Part;
                this.mapping = target.Mapping;
            }

            public Format convert()
            {
                return new Format(part, mapping);
            }

            private IEnumerable getMapping(Port target)
            {
                if (target.inspector != null && target.inspector.type != null && part != null)
                {
                    string partName = part.name.Split(new string[] { " (" }, System.StringSplitOptions.None)[0];
                    PortType portType = target.inspector.type;
                    return EnvironmentUtils.getAssetDropdown<MappingPortPart>()
                     .Where(x =>
                        {
                            MappingPortPart m = x.Value as MappingPortPart;
                            return m.Part.name.Equals(partName) && m.Type.Equals(portType);
                        }
                     );
                }

                return new List<MappingPortPart>();
            }
        }
    }

#endif
}