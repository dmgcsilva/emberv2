﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Linq;
using UnityEngine.AI;

public class RandomGeneratorSync : MonoBehaviour
{
    public PortGrammar[] grammars;
    public GameObject root;
    public int spawn;
    [ValueDropdown("@EnvironmentUtils.getAssetDropdown<ModuleSet>()")]
    public ModuleSet moduleSet;
    [ValueDropdown("@EnvironmentUtils.getAssetDropdown<Plug>()")]
    public Plug closedPlug;
    [Range(0, 1)]
    public float boundsTolerance;
    [Range(0, 1)]
    public float adapterProbablity;
    [Range(0, 1)]
    public float chanceStayInSameTheme;

    private int spawned;

    private Queue<Port> openPorts;
    private List<Tuple<Module, Bounds>> bounds;

    private Transform geometryRoot;
    private GameObject rotator;

    private Dictionary<PortType, List<TaggedModule>> modules;
    private List<Module> adapters;

    private Dictionary<string, List<GameObject>> activeElements;

    private bool finishedInstantiation = false;

    [System.NonSerialized, ShowInInspector]
    public bool finished = false;

    void Awake()
    {
        spawned = 1;

        openPorts = new Queue<Port>();
        bounds = new List<Tuple<Module, Bounds>>();

        geometryRoot = transform;
        rotator = new GameObject("Rotator");
        
        mapModuleSet();
        adapters = new List<Module>();

        activeElements = new Dictionary<string, List<GameObject>>();

        GameObject op = Instantiate(root, new Vector3(0, 0, 0), Quaternion.identity);


        op.transform.parent = geometryRoot;

        Transform ports = op.transform.Find("Ports");

        for (int i = 0; i < ports.childCount; i++)
        {
            Port port = ports.GetChild(i).GetComponent<Port>();
            if (port.Type.Direction.Equals(Direction.Horizontal))
                openPorts.Enqueue(port);
        }

        bounds.Add(new Tuple<Module,Bounds>(op.GetComponent<Module>(), getBounds(op)));

        nextModule();
    }

    private void Update()
    {
        if(!finished && finishedInstantiation)
        {
            NavMeshSurface surface = gameObject.GetComponent<NavMeshSurface>();
            surface.BuildNavMesh();
            finished = true;
        }
    }

    void nextModule()
    {
        if(spawned >= spawn || openPorts.Count == 0)
        {
            finalize();
            return;
        }

        Port previousPort = openPorts.Dequeue();
        Module previousModule = previousPort.transform.parent.parent.GetComponent<Module>();

        List<PortConnectionRule> applicableRules = new List<PortConnectionRule>();

        foreach (PortGrammar grammar in grammars)
        {
            foreach (PortConnectionRule rule in grammar.ConnectionRules)
            {
                if (previousPort.Type.Equals(rule.PortA))
                {
                    applicableRules.Add(rule);
                }
                else if (previousPort.Type.Equals(rule.PortB))
                {
                    applicableRules.Add(rule.getInverse());
                }
            }
        }

        if (applicableRules.Count < 1)
        {
            Debug.LogError("No rules for port: " + previousPort.name);
        }

        PortConnectionRule chosenRule = getRandomRule(applicableRules, previousModule);

        PortType newPortType = chosenRule.PortB;
        Plug previousPlug = chosenRule.AConnectedByB;
        Plug newPlug = chosenRule.BConnectedToA;


        GameObject newModuleGameObject = Instantiate(getRandomModuleWith(newPortType)) as GameObject;

        List<Port> otherPorts;
        Port newPort = getPort(newModuleGameObject, newPortType, out otherPorts);

        repositionModule(newModuleGameObject, newPort, previousPort);

        Module newModule = newModuleGameObject.GetComponent<Module>();

        if (newModule.IsBounded == false)
        {
            previousModule.addConnection(new Connection(previousPort, newModule));
            newModule.addConnection(new Connection(newPort, previousModule));

            enableParts(previousPort, previousPlug);
            enableParts(newPort, newPlug);

            foreach (Port other in otherPorts)
                openPorts.Enqueue(other);
                
            if (newModule.IsAdapter)
                adapters.Add(newModule);
        }
        else
        {
            Bounds newModuleBounds = getBounds(newModuleGameObject);
            bool intersected = isIntersectingOthers(newModuleBounds);

            if (intersected)
            {
                Destroy(newModuleGameObject);
                enableParts(previousPort, closedPlug);
            }
            else
            {
                previousModule.addConnection(new Connection(previousPort, newModule));
                newModule.addConnection(new Connection(newPort, previousModule));

                bounds.Add(new Tuple<Module, Bounds>(newModule, newModuleBounds));

                enableParts(previousPort, previousPlug);
                enableParts(newPort, newPlug);

                foreach (Port other in otherPorts)
                    openPorts.Enqueue(other);

                if (newModule.IsAdapter)
                    adapters.Add(newModule);

                spawned++;
            }
        }

        nextModule();
    }

    private void repositionModule(GameObject module, Port newPort, Port previousPort)
    {
        rotator.transform.position = newPort.transform.position;
        rotator.transform.rotation = newPort.transform.rotation;

        module.transform.parent = rotator.transform;

        rotator.transform.rotation = new Quaternion(0, 0, 0, 0);

        rotator.transform.position = previousPort.transform.position;
        rotator.transform.rotation = previousPort.transform.rotation * new Quaternion(0, 1, 0, 0);

        module.transform.parent = geometryRoot;
    }

    private bool isIntersectingOthers(Bounds moduleBounds)
    {
        bool intersected = false;
        foreach (var otherModuleBounds in bounds)
        {
            if (moduleBounds.Intersects(otherModuleBounds.Item2))
            {
                intersected = true;
                break;
            }
        }
        return intersected;
    }

    private Bounds getBounds(GameObject moduleGameObject)
    {
        Module module = moduleGameObject.GetComponent<Module>();
        float rotation = module.transform.rotation.eulerAngles.y;

        Vector3 localCenter = new Vector3(0, module.Bounds.y / 2, 0);

        float xSize;
        float ySize = module.Bounds.y * boundsTolerance;
        float zSize;

        if((rotation > 45 && rotation < 135) || (rotation > 225 && rotation < 315))
        {
            xSize = module.Bounds.z * 2 * boundsTolerance;
            zSize = module.Bounds.x * 2 * boundsTolerance;
        }
        else
        {
            xSize = module.Bounds.x * 2 * boundsTolerance;
            zSize = module.Bounds.z * 2 * boundsTolerance;
        }

        var bounds = new Bounds(module.transform.position + localCenter, new Vector3(xSize,ySize,zSize));
        return bounds;
    }


    private Port getPort(GameObject module, PortType portType, out List<Port> others)
    {
        Port result = null;
        others = new List<Port>();
        foreach (Transform portTransform in module.transform.Find("Ports")) 
        {
            Port port = portTransform.GetComponent<Port>();

            if (port.Type.Equals(portType) && result == null)
            {
                result = port;
            }
            else if(port.Type.Direction.Equals(Direction.Horizontal)) 
            {
                others.Add(port);
            }
        }

        if (result != null && others.Count > 0)
            return result;
        else
        {
            Debug.LogError("Module doesn't have the required ports: " + module.name);
            throw new Exception();
        }
    }


    private PortConnectionRule getRandomRule(List<PortConnectionRule> applicableRules, Module module)
    {
        PortConnectionRule chosenRule;
        bool accepted = false;

        if (!module.IsAdapter)
        {
            do
            {
                chosenRule = applicableRules[UnityEngine.Random.Range(0, applicableRules.Count)];
                if (!chosenRule.PortB.IsAdapterPort)
                    accepted = true;
                else if (UnityEngine.Random.Range(0.0f, 1.0f) < adapterProbablity)
                    accepted = true;
            }
            while (!accepted);
        }
        else
        {
            PortType previousPortType = null;
            Module previousModule = module.Connections[0].Module;
            foreach (Connection connection in previousModule.Connections)
            {
                if (connection.Module.gameObject.Equals(module.gameObject))
                    previousPortType = connection.Port.Type;
            }

            do
            {
                chosenRule = applicableRules[UnityEngine.Random.Range(0, applicableRules.Count)];
                string nameNewPort = chosenRule.PortB.name;
                string namePreviousPort = previousPortType.name;
                if (!nameNewPort.Contains(namePreviousPort) && !namePreviousPort.Contains(nameNewPort))
                    accepted = true;
                else if(UnityEngine.Random.Range(0.0f, 1.0f) < chanceStayInSameTheme)
                    accepted = true;
            }
            while (!accepted);
        }

        return chosenRule;
    }

    private GameObject getRandomModuleWith(PortType portType)
    {
        List<TaggedModule> tmp = new List<TaggedModule>();

        modules.TryGetValue(portType, out tmp);

        if(tmp == null || tmp.Count == 0)
        {
            Debug.LogError("No module haves the port type: " + portType.name);
            throw new Exception();
        }
            
        return tmp[UnityEngine.Random.Range(0, tmp.Count)].Module;    
    }

    private void enableParts(Port port, Plug plug)
    {
        foreach (Format format in port.Formats)
        {
            DynamicPart dynamicPart = format.Part.GetComponent<DynamicPart>();

            foreach (Element element in dynamicPart.GetComponentsInChildren<Element>(true))
            {
                element.gameObject.SetActive(false);

                if (element.CanOverlap == false)
                {
                    string elementName = element.gameObject.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];

                    List<GameObject> elements;
                    if (activeElements.TryGetValue(elementName, out elements))
                    {
                        foreach (GameObject otherElement in elements)
                        {
                            if (Vector3.Distance(element.transform.position, otherElement.transform.position) <= 0.1f)
                                otherElement.gameObject.SetActive(false);
                        }
                    }
                }
            }

            foreach (MappingPortPart.Entry entry in format.Mapping.Entrys)
            {
                if (!entry.Plug.Equals(plug))
                    continue;

                foreach (Shape shape in dynamicPart.Shapes)
                {
                    if (!shape.Name.Equals(entry.Shape))
                        continue;

                    foreach (GameObject element in shape.Elements)
                    {
                        element.SetActive(true);

                        string elementName = element.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];

                        List<GameObject> elements;
                        activeElements.TryGetValue(elementName, out elements);
                        if (elements == null)
                        {
                            elements = new List<GameObject>();
                            activeElements.Add(elementName, elements);
                        }

                        elements.Add(element);
                    }
                }
            }
        }
    }

    private void mapModuleSet()
    {
        modules = new Dictionary<PortType, List<TaggedModule>>();
        foreach (TaggedModule taggedModule in moduleSet.Modules)
        {
            foreach (TaggedPort port in taggedModule.Ports)
            {
                List<TaggedModule> taggedModules;
                if (!modules.TryGetValue(port.PortType, out taggedModules))
                {
                    taggedModules = new List<TaggedModule>();
                    modules.Add(port.PortType, taggedModules);
                }

                taggedModules.Add(taggedModule);
            }
        }
    }

    void finalize()
    {
        Destroy(rotator);
        
        foreach (Port port in openPorts)
        {
            enableParts(port, closedPlug);
        }

        foreach (Module adapter in adapters)
        {
            if(adapter.Connections.Count == 1)
            {
                Module otherModule = adapter.Connections[0].Module;

                Connection connection = otherModule.removeConnection(adapter);

                enableParts(connection.Port, closedPlug);

                Destroy(adapter.gameObject);
            }
        }

        Debug.Log("Spawned:" + spawned);

        finishedInstantiation = true;
    }
}
