﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KirinSpawner : MonoBehaviour
{
    public int number;

    public GameObject kirinPrefab;

    private RandomGeneratorSync gen;

    private bool spawned = false;

    void Start()
    {
        gen = gameObject.GetComponent<RandomGeneratorSync>();
        if(gen == null)
        {
            instatiateKirin();
            spawned = true;
        }
    }

    void Update()
    {
        if(!spawned && gen.finished)
        {
            instatiateKirin();
            spawned = true;
        }        
    }

    Vector3 selectRandomPosition()
    {
        int module = UnityEngine.Random.Range(0, transform.childCount);
        return transform.GetChild(module).position;
    }

    void instatiateKirin()
    {
        for (int i = 0; i < number; i++)
        { 
            Vector3 postion = selectRandomPosition();
            GameObject kirin = Instantiate(kirinPrefab, postion, Quaternion.identity);
            kirin.transform.parent = transform;
        }
    }
}
